const Discord = require('discord.js');
const bot     = new Discord.Client(); 

const token   = 'NzMyNDA0NDczNzAzNDMyMzI0.Xw0G4g.tWms7RipETen8yWPTrPZB2104iE';

const PREFIX  = '!';

const ytdl = require("ytdl-core");

var servers = {};

var destek_caqirma;
var qeydiyyat_caqirma;

//channel
var destek_caqirma_id       = "732232439191306251";
var qeydiyyat_caqirma_id    = "732042186115383366";
var command_chat_id         = "732349942433185814";

//role
var vetensash_id        = "732038565827051601";
var qonaq_id            = "732030749649403905";
var destek_heyeti_id    = "732251851398971452";
var admin_id            = "732531998287200316";
var bots_id             = "732040848413556736";
var everyone_id         = "731675517563961465";
var polis_id            = "732238899551928330";
var sehiyye_id          = "732245589345042443";
var taksi_id            = "732260927910576168";

var user_list;
var bab_in_channel = false;

//status
var online_user_count   = 0;
var offline_user_count  = 0;

const xah_obj_to_map = ( obj => {
    const mp = new Map;
    Object.keys ( obj ). forEach (k => { mp.set(k, obj[k]) });
    return mp;
});

const get_users_status = ( obj => {
    online_user_count   = 0;
    offline_user_count  = 0;
    users = bot.users;
    var user_list = xah_obj_to_map(users).get("cache");

    for (var [key, value] of user_list) {
        if(value.presence.status === "offline"){
            offline_user_count = offline_user_count + 1;
        }else{
            online_user_count = online_user_count + 1;
        }
    }
    bot.channels.cache.get("732337926918242334").setName("🌍 All Members: " + (online_user_count+offline_user_count));
    bot.channels.cache.get("732337929585819790").setName("✅ Online Members: " + online_user_count);
    bot.channels.cache.get("732617822496227419").setName("❌ Offline Members: " + offline_user_count);
});

bot.on('ready', () =>{
    console.log('BAB is online!');

    try {
        destek_caqirma      = bot.channels.cache.get(destek_caqirma_id);
        qeydiyyat_caqirma   = bot.channels.cache.get(qeydiyyat_caqirma_id);

        get_users_status();
        updateUserCount(60);
    } catch (error) {   
    }
});

bot.on('guildMemberAdd', member => {
    try {
        var role = member.guild.roles.cache.get("732030749649403905");
        member.roles.add(role);
    } catch (error) {   
    }
});

bot.on('message',async message=>{
    try {
        let args = message.content.split(" ");
        is_bots = message.member.roles.cache.has(bots_id);
        if(is_bots){
            return;
        }

        switch(args[0]){
            case '!destek':
                is_vetensash = message.member.roles.cache.has(vetensash_id);
                if(is_vetensash){
                    if(message.channel.id === destek_caqirma_id){
                        message.reply("Salam şəhərimizin üzvü. <@&" + destek_heyeti_id + "> sizinlə maraqlanacaq."); 
                    } else {
                        bot.channels.cache.get(message.channel.id).send("Zəhmət olmasa " + destek_caqirma.toString() + " otağına müraciət edin!");
                    }
                } else {
                    message.reply("vətəndaş olmadan dəstək heyyətini cağıra bilməzsiniz!");
                }
                break;
            case '!qeydiyyat':
                is_qonaq = message.member.roles.cache.has(qonaq_id);
                if(is_qonaq){
                    if(message.channel.id === qeydiyyat_caqirma_id){
                        message.reply("Salam şəhərimizin yeni üzvü. <@&" + destek_heyeti_id + "> sizinlə maraqlanacaq."); 
                    } else {
                        bot.channels.cache.get(message.channel.id).send("Zəhmət olmasa " + qeydiyyat_caqirma.toString() + " otağına müraciət edin!");
                    }
                } else {
                    message.delete({ timeout: 1000 });
                }
                break;
            case '!ip':
                is_qonaq = message.member.roles.cache.has(qonaq_id);
                if(is_qonaq == false){
                    message.channel.send("```ip: connect 46.102.106.19\nts: 185.153.228.35:9987```");
                } else {
                    message.delete({ timeout: 1000 });
                }
                break;
            case '!active':

                break;
            case '!play':
                is_admin = message.member.roles.cache.has(admin_id);
                if(is_admin){
                    function play(connection, message){
                        var server = servers[message.guild.id];
                        server.dispatcher = connection.play(ytdl(server.queue[0],{filter: "audioonly"} ));
                        server.dispatcher.setVolume(0.5);
                        server.queue.shift();
                        server.dispatcher.on("finish",function(){
                            if(server.queue[0]){
                                play(connection,message);
                            } else {
                                bab_in_channel = false;
                                connection.disconnect();
                            }
                        })
                    }

                    if(!args[1]){
                        message.channel.send("Zehmet olmasa '''!play [youtube url]''' sheklinde daxil edin.");
                        console.log("parametr yoxdur!!!");
                        return;
                    }
                    if(!message.member.voice.channel) {
                        message.channel.send("Zehmet olmasa otaqa daxil olun.");
                        console.log("otaqda deilsen!!!")
                        return;
                    }
                    if(!servers[message.guild.id]) {
                        servers[message.guild.id] = {
                            queue: []
                        }
                    }
                    var server = servers[message.guild.id];
                    server.queue.push(args[1]);

                    //console.log(server.queue.length);

                    if(bab_in_channel == false) {
                        bab_in_channel = true;
                        message.member.voice.channel.join().then(function(connection){
                            play(connection, message);
                        });
                    }
                }
                break;
            case '!skip':
                is_admin = message.member.roles.cache.has(admin_id);
                if(is_admin){
                    var server = servers[message.guild.id];
                    if(server.dispatcher) {   
                        server.dispatcher.end();
                    }
                    if(!server.dispatcher){
                        bab_in_channel = false;
                    }
                    message.channel.send("Skipping the song!");
                }
                break;
            case '!stop':
                is_admin = message.member.roles.cache.has(admin_id);
                if(is_admin){
                    var server = servers[message.guild.id];
                    if(message.guild.voice.connection){
                        for(var i = server.queue.length -1; i >= 0; i--){
                            server.queue.splice(i,1);
                        }
                        server.dispatcher.end();
                        message.channel.send("Ending the queue Leaving the voice channel!")
                        console.log('stopped the queue')
                        bab_in_channel = false;
                        message.guild.voice.connection.disconnect();
                    }
                }
                break;
            case '!pause':
                is_admin = message.member.roles.cache.has(admin_id);
                if(is_admin){
                    var server = servers[message.guild.id];
                    if(server.dispatcher) server.dispatcher.pause();
                    message.channel.send("Pause the song!");
                }
                break;
            case '!resume':
                is_admin = message.member.roles.cache.has(admin_id);
                if(is_admin){
                    var server = servers[message.guild.id];
                    if(server.dispatcher) server.dispatcher.resume();
                    message.channel.send("Resume the song!");
                }
                break;
            case '!help':
                is_admin = message.member.roles.cache.has(admin_id);
                is_vetensash = message.member.roles.cache.has(vetensash_id);
                is_qonaq = message.member.roles.cache.has(qonaq_id);
                if(is_admin){
                    message.channel.send("```Music BOT \n!play [youtube url] \n!pause \n!resume \n!skip \n!stop \n!restart music ```");
                    message.channel.send("```!clear [1-100]```");
                    message.channel.send("```!report size```");
                }
                if(is_vetensash){
                    message.channel.send("```!destek -> Dəstək heyyətini caqırmaq ücün.```");
                    message.channel.send("```!ip -> Serverin ip'sini öyrənmək ücün.```");
                }
                if(is_qonaq){
                    message.channel.send("```!qeydiyyat -> Dəstək heyyətini caqırmaq üçün.```");
                }
                break
            case '!clear':
                is_admin = message.member.roles.cache.has(admin_id);
                if(is_admin){
                    async function purge(){
                        message.delete();
                        const fetched = await message.channel.messages.fetch({limit: args[1]});
                        console.log(fetched.size + ' messages found, deleting...');

                        message.channel.bulkDelete(fetched).catch(error => message.channel.send(`Error: ${error}`));
                    }
                    purge();
                }
                break;
            case '!report':
                is_admin = message.member.roles.cache.has(admin_id);
                if(is_admin && args[1] === 'size'){
                    var qonaq_size = message.guild.roles.cache.get(qonaq_id).members.size;
                    var vetensash_size = message.guild.roles.cache.get(vetensash_id).members.size;
                    var everyone_size = message.guild.roles.cache.get(everyone_id).members.size;
                
                    var polis_size = message.guild.roles.cache.get(polis_id).members.size;
                    var sehiyye_size = message.guild.roles.cache.get(sehiyye_id).members.size;
                    var taksi_size = message.guild.roles.cache.get(taksi_id).members.size;

                    var admin_size = message.guild.roles.cache.get(admin_id).members.size;
                    var destek_heyeti_size = message.guild.roles.cache.get(destek_heyeti_id).members.size;

                    message.channel.send("```Qonaq:     "+qonaq_size+"\nVetensash: "+vetensash_size+"\nEveryone:  "+everyone_size+"```");
                    message.channel.send("```Polis:   "+polis_size+"\nSehiyye: "+sehiyye_size+"\nTaksi:   "+taksi_size+"```");
                    message.channel.send("```Staff:         "+admin_size+"\nDestek Heyeti: "+destek_heyeti_size+"```");
        
                }
                break;
            case '!restart':
                is_admin = message.member.roles.cache.has(admin_id);
                if(is_admin && args[1] === 'music'){
                    console.log("aaa");
                    bab_in_channel = false;
                    var server = servers[message.guild.id];
                    if(message.guild.voice.connection){
                        for(var i = server.queue.length -1; i >= 0; i--){
                            server.queue.splice(i,1);
                        }
                        server.dispatcher.end();
                        message.channel.send("Ending the queue Leaving the voice channel!");
                        console.log('stopped the queue');
                        bab_in_channel = false;
                        message.guild.voice.connection.disconnect();
                    } 
                }
                break;
        }
    } catch (error) {   
    }
});

const updateUserCount = (seconds)  => {
    setInterval(function(){
        get_users_status();
        }, seconds * 1000);
}

bot.login(token);